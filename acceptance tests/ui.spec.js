
'use strict'

const puppeteer = require('puppeteer')
const { configureToMatchImageSnapshot } = require('jest-image-snapshot')
const PuppeteerHar = require('puppeteer-har')

const width = 800
const height = 600
const delayMS = 10

let browser
let page
let har
// threshold is the difference in pixels before the snapshots dont match
const toMatchImageSnapshot = configureToMatchImageSnapshot({
	customDiffConfig: { threshold: 2 },
	noColors: true,
})
expect.extend({ toMatchImageSnapshot })

describe('create user', () => {
	beforeAll( async() => {
		browser = await puppeteer.launch({ headless: true, slowMo: delayMS, args: [`--window-size=${width},${height}`] })
		page = await browser.newPage()
		har = new PuppeteerHar(page)
		await page.setViewport({ width, height })
	})

	test('create account', async done => {

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })

		await page.screenshot({ path: 'screenshots/empty_list.png' })

		await page.type('input[name=user]', 'bread4')
		await page.type('input[name=pass]', 'pepomen1')
		await page.type('input[name=confirm]', 'pepomen1')
		await page.click('input[type=submit]')
		page.waitForNavigation(), // wait for the page to load

		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('Log in')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('Sign In')

		const username = await page.evaluate( () => {
			const dom = document.querySelector('input[name=user]')
			const selectedValue = dom.value
			return selectedValue
		})

		const password = await page.evaluate( () => {
			const dom = document.querySelector('input[name=pass]')
			const selectedValue = dom.value
			return selectedValue
		})

		expect(username).toBe('')
		expect(password).toBe('')

		const image = await page.screenshot()
		expect(image).toMatchImageSnapshot()
		done()
	}, 16000)


	test('create account without 6 sybmol password', async done => {

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })

		await page.screenshot({ path: 'screenshots/empty_list.png' })

		await page.type('input[name=user]', 'bread4')
		await page.type('input[name=pass]', 'pepo')
		await page.type('input[name=confirm]', 'pepo')
		await page.click('input[type=submit]')

		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('ERROR')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('Passwords needs to be minimum 6 symbols')
		done()
	}, 16000)


	test('create account with different passwords', async done => {

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })

		await page.screenshot({ path: 'screenshots/empty_list.png' })

		await page.type('input[name=user]', 'bread4')
		await page.type('input[name=pass]', '123321')
		await page.type('input[name=confirm]', '321123')
		await page.click('input[type=submit]')

		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('ERROR')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('Passwords don`t match')
		done()
	}, 16000)

	test('create account without username', async done => {

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })

		await page.screenshot({ path: 'screenshots/empty_list.png' })

		await page.type('input[name=user]', '')
		await page.type('input[name=pass]', '123321')
		await page.type('input[name=confirm]', '123321')
		await page.click('input[type=submit]')

		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('ERROR')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('missing username')
		done()
	}, 16000)
    
    
	test('login in account', async done => {

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })


		await page.type('input[name=user]', 'bread6')
		await page.type('input[name=pass]', 'pepomen1')
		await page.type('input[name=confirm]', 'pepomen1')
		await page.click('input[type=submit]')
		//     page.waitForNavigation(),
		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('Log in')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('Sign In')

		const username = await page.evaluate( () => {
			const dom = document.querySelector('input[name=user]')
			const selectedValue = dom.value
			return selectedValue
		})

		const password = await page.evaluate( () => {
			const dom = document.querySelector('input[name=pass]')
			const selectedValue = dom.value
			return selectedValue
		})

		expect(username).toBe('')
		expect(password).toBe('')
        
		await page.screenshot({ path: 'screenshots/empty_login.png' })

		await page.type('input[name=user]', 'bread6')
		await page.type('input[name=pass]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')
		//      page.waitForNavigation()

		const title1 = await page.title()
		expect(title1).toBe('Home Page')

		const greetings = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings).toBe('Hello bread6, your playlists are:')
		done()
	}, 16000)
	/////////////////////////////////////////////////////////////////////
	test('login without valid account', async done => {

		await page.goto('http://localhost:8080/login', { timeout: 30000, waitUntil: 'load' })


		await page.type('input[name=user]', 'user1')
		await page.type('input[name=pass]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('ERROR')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(heading).toBe('An Error Has Occurred')

		done()
	}, 16000)
})


describe('create playlist', () => {
	
	test('creating playlist', async done => {
        await page.goto('http://localhost:8080/logout', { timeout: 30000, waitUntil: 'load' })
		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })


		await page.type('input[name=user]', 'bread14')
		await page.type('input[name=pass]', 'pepomen1')
		await page.type('input[name=confirm]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('Log in')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(heading).toBe('Sign In')

		const username = await page.evaluate( () => {
			const dom = document.querySelector('input[name=user]')
			const selectedValue = dom.value
			return selectedValue
		})

		const password = await page.evaluate( () => {
			const dom = document.querySelector('input[name=pass]')
			const selectedValue = dom.value
			return selectedValue
		})

		expect(username).toBe('')
		expect(password).toBe('')
    
		await page.screenshot({ path: 'screenshots/empty_login.png' })

		await page.type('input[name=user]', 'bread14')
		await page.type('input[name=pass]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')

		const title1 = await page.title()
		expect(title1).toBe('Home Page')

		const greetings = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings).toBe('Hello bread14, your playlists are:')

		await page.goto('http://localhost:8080/playlist', { timeout: 30000, waitUntil: 'load' })



		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Create Playlist')

		await page.type('input[name=playlist]', 'playlist')


		const input = await page.$('input[name=image]')
		await input.uploadFile('./public/avatars/avatar.png')

		await page.click('input[type=submit]')

		const greetings1 = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings1).toBe('Hello bread14, your playlists are:')
		/////////////////////////////////////////1

		done()
	}, 16000)

	test('uploading music to playlist', async done => {

		await page.goto('http://localhost:8080/upload', { timeout: 30000, waitUntil: 'load' })

		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Upload music')


		const input = await page.$('input[name=music]')
		await input.uploadFile('./public/music/music.mp3')

		await page.click('input[type=submit]')

		const greetings1 = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings1).toBe('Hello bread14, your playlists are:')



		done()
	}, 16000)

	test('uploading music to playlist with incorrect file type', async done => {

		await page.goto('http://localhost:8080/upload', { timeout: 30000, waitUntil: 'load' })

		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Upload music')


		const input = await page.$('input[name=music]')
		await input.uploadFile('./public/avatars/avatar.png')

		await page.click('input[type=submit]')

		const greetings1 = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings1).toBe('Your file format is".png", Please select a .Mp3 file')



		done()
	}, 16000)




	/////////////////////////////////////////////
	test('try creating playlist without name', async done => {

		await page.goto('http://localhost:8080/playlist', { timeout: 30000, waitUntil: 'load' })

		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Create Playlist')


		const input = await page.$('input[name=image]')
		await input.uploadFile('./public/avatars/avatar.png')

		await page.click('input[type=submit]')

		const greetings1 = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings1).toBe('missing playlist name')

		done()
	}, 16000)

	test('try creating playlist without image', async done => {

		await page.goto('http://localhost:8080/playlist', { timeout: 30000, waitUntil: 'load' })

		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Create Playlist')

		await page.type('input[name=playlist]', 'playlist')

		await page.click('input[type=submit]')


		const greetings1 = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings1).toBe('cannot find image')


		done()
	}, 16000)
})








describe('onine playlist', () => {


	test('access onine playlists', async done => {
		await page.goto('http://localhost:8080/logout', { timeout: 30000, waitUntil: 'load' })

		await page.goto('http://localhost:8080/register', { timeout: 30000, waitUntil: 'load' })


		await page.type('input[name=user]', 'user1')
		await page.type('input[name=pass]', 'pepomen1')
		await page.type('input[name=confirm]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')

		const title = await page.title()
		expect(title).toBe('Log in')

		const heading = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText 
		})
		expect(heading).toBe('Sign In')

		const username = await page.evaluate( () => {
			const dom = document.querySelector('input[name=user]')
			const selectedValue = dom.value
			return selectedValue
		})

		const password = await page.evaluate( () => {
			const dom = document.querySelector('input[name=pass]')
			const selectedValue = dom.value
			return selectedValue
		})

		expect(username).toBe('')
		expect(password).toBe('')
    
		await page.screenshot({ path: 'screenshots/empty_login.png' })

		await page.type('input[name=user]', 'user1')
		await page.type('input[name=pass]', 'pepomen1')
		await page.click('input[type=submit]')
		await page.waitForSelector('title')

		const title1 = await page.title()
		expect(title1).toBe('Home Page')

		const greetings = await page.evaluate( () => {
			const dom = document.querySelector('h2')
			return dom.innerText
		})
		expect(greetings).toBe('Hello user1, your playlists are:')

		await page.goto('http://localhost:8080/allPlaylists', { timeout: 30000, waitUntil: 'load' })



		const playlist = await page.evaluate( () => {
			const dom = document.querySelector('h1')
			return dom.innerText
		})
		expect(playlist).toBe('Online Playlists')

		const onlinePlayist = await page.evaluate( () => {
			const dom = document.querySelector('.img a')
			return dom.innerText
		})
		expect(onlinePlayist).toBe('User: bread14')


		await browser.close()


		done()
	}, 16000)
})